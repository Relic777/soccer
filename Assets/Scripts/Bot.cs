﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bot : MonoBehaviour {

    public Transform ball;
    public float speed = 5.3f;

    private CharacterController controller;
    private Transform thisTransform;

	// Use this for initialization
	void Start () {
        thisTransform = transform;
        controller = gameObject.GetComponent<CharacterController>();
    }
	
	// Update is called once per frame
	void Update () {
        var offset = ball.position - thisTransform.position;

        if (offset.magnitude > 1.1f)
        {
            offset = offset.normalized * speed;
            offset.y = 0;

            controller.Move(offset * Time.deltaTime);
        }
    }
}
