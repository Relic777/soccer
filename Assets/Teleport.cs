using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Teleport : MonoBehaviour {

	public bool teleported = false;
	public Teleport target;
    public Teleport target2;
    public Text ScoreText;
    public int score;

    void OnTriggerEnter (Collider other)
	{
		if (other.CompareTag("ball"))
		{
			if (!teleported)
			{
				target.teleported = true;
				other.gameObject.transform.position = target.gameObject.transform.position;
                //ScoreText.text = "" + score;
			}
		}
	}
	
	void OnTriggerExit (Collider other)
	{
		if(other.CompareTag("ball"))
		{
			teleported = false;
		}
	}
}
